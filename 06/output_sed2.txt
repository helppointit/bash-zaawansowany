The grand old Duke of York
He had ten thousand men
He marched $TUm up to the top of the hill
And he marched $TUm down again
And when $TUy were up they were up
And when $TUy were down they were down
And when $TUy were only half-way up
They were nei$TUr up nor down
