#!/bin/bash
shopt -s extglob
ALLRAW=0
NIKRAW=0
NIKSUP=0
CANRAW=0
CANSUP=0
CANJPG=0
OTHER=0
for file in $(ls *); do
	case $file in
		@(IMG|DSC)*@(@(.CR2|.NEF).xmp|.NEF|.CR2) ) 
			(( ALLRAW++ )) ;;&
		DSC*@(.NEF|.NEF.xmp) ) 
			(( NIKRAW++ )) ;;&
		DSC*@(.NEF.xmp) ) 
			(( NIKSUP++ )) ;;
		IMG*@(.CR2|.CR2.xmp) ) 
			(( CANRAW++ )) ;;&
		IMG*@(.CR2.xmp) ) 
			(( CANSUP++ )) ;;
		IMG*@(.JPG|.jpg) ) 
			(( CANJPG++ )) ;;
		*) (( OTHER++ ))
	esac
done

echo $ALLRAW
echo $NIKRAW
echo $NIKSUP
echo $CANRAW
echo $CANSUP
echo $CANJPG
echo $OTHER
